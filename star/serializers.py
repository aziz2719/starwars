from star.models import CSV, SaveCSV
from rest_framework import serializers


class FileUploadSerializer(serializers.Serializer):
    file = serializers.FileField()

class SaveFileSerializer(serializers.Serializer):
    
    class Meta:
        model = CSV
        fields = "__all__"
