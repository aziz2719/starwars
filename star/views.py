from lib2to3.pgen2.parse import ParseError
from xml.etree.ElementTree import ParseError
from django.shortcuts import render
from rest_framework import generics
import io, csv, pandas as pd
from rest_framework.response import Response
import requests
from rest_framework.views import APIView
from rest_framework import status, viewsets
from rest_framework.response import Response
from datetime import datetime
import csv
import pandas
from .models import SaveCSV ,CSV
from .serializers import FileUploadSerializer, SaveFileSerializer



count = {'count': 0, "next_page": 1, 'dates': [],}

count_planet = {'count_planet': 0, 'homeworld': 1}

starwars = [count, ]

starwars_planet = [count_planet, ]

star = []

# import sys import csv reader = csv.reader(open("files.csv"), delimiter=";") for id, path, title, date, author, platform, type, port in reader: print date
# data.sort_values(["Age"], axis=0, ascending=[False], inplace=True) 
class PeopleView(generics.ListCreateAPIView):
    serializer_class = FileUploadSerializer

    def get(self, request):
        page = request.query_params.get('page', 1)
        if starwars[0]['next_page'] == int(page):
            url = f'https://swapi.dev/api/people/?page={page}'
            response = requests.get(url).json()
            starwars.append(response['results'])
            starwars[0]['count'] += 10
            starwars[0]['next_page'] += 1
            starwars[0]['dates'].append(datetime.today())
            with open('story.csv', 'w', encoding='utf-8') as file:
                file.write(str(starwars[0]))
                file.write('\n')
                #csv.DictReader(file, delimiter=';')
                for i in range(1, len(starwars)):
                    for j in starwars[i]:
                        file.write(str(j))
                        file.write('\n')
            return Response(starwars, status=status.HTTP_200_OK)
        return Response({"error": "Enter valid page"})

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        file = serializer.validated_data['file']
        reader = pd.read_csv(file, skiprows=2, header=None)
        print(reader)
        for _, row in reader.iterrows():
            new_file = CSV(
                       name = row['name'],
                       height= row["height"],
                       mass= row["mass"],
                       hair_color= row["hair_color"],
                       skin_color= row["row"],
                       eye_color= row["eye_color"],
                       birth_year= row["birth_year"],
                       gender= row["gender"],
                       homeworld= row["homeworld"],
                       films= row["films"],
                       species= row["species"],
                       vehicles= row["vehicles"],
                       starships= row["starships"],
                       craeted= row["created"],
                       edited= row["edited"],
                       url= row["url"],
                       )
            new_file.save()
        return Response({"status": "success"},
                        status.HTTP_201_CREATED)


    # def csv_file_parser(file):
    #     print('dvgifugbjdnkrg')
    #     if PeopleView:
    #         result_dict = {}
    #         with open(file) as csvfile:
    #             reader = csv.DictReader(csvfile)
    #             line_count = 1
    #             for rows in reader:
    #                 for key, value in rows.items():
    #                     if not value:
    #                         raise ParseError('Missing value in file. Check the {} line'.format(line_count))
    #                 result_dict[line_count] = rows
    #                 line_count += 1

    #         print(result_dict)
    #         return result_dict


# class StarView(APIView):

#     def get(self, request):
#         page = request.query_params.get('page', 1)
#         if starwars[0]['next_page'] == int(page):
#             url = f'https://swapi.dev/api/people/?page={page}'
#             response = requests.get(url).json()
#             starwars.append(response['results'])
#             starwars[0]['count'] += 10
#             starwars[0]['next_page'] += 1
#             starwars[0]['dates'].append(datetime.today())
#             with open('story.csv', 'w') as file:
#                 file.write(str(starwars[0]))
#                 for i in range(1, len(starwars)):
#                     for j in starwars[i]:
#                         file.write('\n')
#                         file.write(str(j))
#             return Response(starwars, status=status.HTTP_200_OK)
#         return Response({"error": "Enter valid page"})


class StarPlanetView(APIView):
    
    def get(self, request):
        page = request.query_params.get('page', 1)
        url_planet = f'https://swapi.dev/api/people/?page={page}'
        response = requests.get(url_planet).json()
        starwars_planet.append(response['results']['homeworld'])
        starwars_planet[0]['count_planet'] += 1

        with open('planet.json', 'w') as file:
            file.write(str(starwars_planet[0]))
            file.write('\n\n')
            for i in range(1, len(starwars_planet)):
                for j in starwars_planet[i]:
                    file.write(str(j))
                    file.write('\n')
        return Response(starwars_planet, status=status.HTTP_200_OK)




    # def get(self, request):
    #     page = request.query_params.get('page', 1)
    #     #if starwars_planet[0]['homeworld'] == int(page):
    #     url_planet = f'https://swapi.dev/api/people/?page={page}'
    #     response = requests.get(url_planet).json()
    #     starwars_planet.append(response['results'])
    #     starwars_planet[0]['count_planet'] += 1
    #     with open('planet.json', 'w') as file:
    #         file.write(str(starwars_planet[0]))
    #         file.write('\n\n')
    #         for i in range(1, len(starwars_planet)):
    #             for j in starwars_planet[i]:
    #                 file.write(str(j))
    #                 file.write('\n')
    #     return Response(starwars_planet, status=status.HTTP_200_OK)



# class StarView(APIView):

#     def get(self, request):
#         page = request.query_params.get('page', 1)
#         url = f'https://swapi.dev/api/people/?page={page}'
#         response = requests.get(url).json()
#         star.append(response['results'])
#         #star.sort(star)
#         with open('star.csv', 'w') as file:     
#             # file.write(str(star))
#             # file.write('\n')
#             for i in range(1, len(star)):
#                 for j in star[i]:
#                     file.write(str(j))
#                     file.write('\n')
#         return Response(star, status=status.HTTP_200_OK)

# data = csv.reader(open('/home/ubuntu/data.csv'),delimiter=',')
#             data = sorted(data, key=operator.itemgetter(2))  


# class CsvToDatabase(APIView):

#     def post(self, request, format=None):
#         data = request.data
#         for key, people in data.items():
#             People(
#             name=people['name'],
#             height=people['height'],
#             mass=people['mass'],
#             hair_color=people['hair_color'],
#             skin_color=people['skin_color'],
#             eye_color=people['eye_color'],
#             birth_year=people['birth_year'],
#             gender=people['gender'],
#             homeworld=people['homeworld'],
#             films=people['films'],
#             species=people['species'],
#             vehicles=people['vehicles'],
#             starships=people['starships'],
#             created=people['created'],
#             edited=people['edited'],
#             url=people['url']
#         ).save()

#         return Response({'received data': request.data})






























# class PeopleView(APIView):
#     serializer_class = CSVSerializer
    
#     def get(self, request):
#         page = request.query_params.get('page', 1)
#         if starwars[0]['next_page'] == int(page):
#             url = f'https://swapi.dev/api/people/?page={page}'
#             response = requests.get(url).json()
#             starwars.append(response['results'])
#             starwars[0]['count'] += 10
#             starwars[0]['next_page'] += 1
#             starwars[0]['dates'].append(datetime.today())
#             with open('story.csv', 'w') as file:
#                 file.write(str(starwars[0]))
#                 file.write('\n')
#                 #csv.DictReader(file, delimiter=';')
#                 for i in range(1, len(starwars)):
#                     for j in starwars[i]:
#                         file.write(str(j))
#                         file.write('\n')
#             return Response(starwars, status=status.HTTP_200_OK)
#         return Response({"error": "Enter valid page"})

