from django.urls import path, include

from rest_framework.routers import DefaultRouter

from star.views import PeopleView, StarPlanetView


router = DefaultRouter()
urlpatterns = [
    path('star/', PeopleView.as_view()),
    path('star_planet', StarPlanetView.as_view()),
]

urlpatterns += router.urls
